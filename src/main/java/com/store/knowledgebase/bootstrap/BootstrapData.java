package com.store.knowledgebase.bootstrap;

import com.store.knowledgebase.domain.Author;
import com.store.knowledgebase.domain.Book;
import com.store.knowledgebase.repository.AuthorRepository;
import com.store.knowledgebase.repository.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootstrapData implements CommandLineRunner {

    private BookRepository bookRepository;
    private AuthorRepository authorRepository;

    public BootstrapData(BookRepository bookRepository, AuthorRepository authorRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
    }

    @Override
    public void run(String... args) throws Exception {

        // First Book & Author
        Author author1 = new Author("Yashavant", "Karnetkar");
        Book book1 = new Book("Let us C", "ISBNLC412", 420.00F);
        book1.getAuthors().add(author1);
        bookRepository.save(book1);


        // Second Book & Author
        Author author2 = new Author("Paulo", "Coelho");
        Book book2 = new Book("The Alchemist", "ISBNTA834", 250.00F);
        book2.getAuthors().add(author2);
        bookRepository.save(book2);


        // Fetch the books and Authors.
        System.out.format("Total books loaded in system : %d%n", bookRepository.count());

        // Print all the books
        System.out.println(bookRepository.findAll());
    }
}
