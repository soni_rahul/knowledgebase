package com.store.knowledgebase.web.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Data
public class KBException extends RuntimeException {

    private KBExceptionEnum KBExceptionEnum;

    public KBException(KBExceptionEnum KBExceptionEnum) {
        this.KBExceptionEnum = KBExceptionEnum;
    }

    public KBExceptionResponse getExceptionResponse(String details) {
        return new KBExceptionResponse(KBExceptionEnum.getReasonPhrase(),
                KBExceptionEnum.getKbStatusCode(), KBExceptionEnum.getHttpStatusCode(), details);
    }

    @Data
    public class KBExceptionResponse {
        private String message;
        private Date date;
        private int customStatusCode;
        private HttpStatus httpStatus;
        private String details;

        public KBExceptionResponse(String message, int customStatusCode, HttpStatus httpStatus, String details) {
            this.message = message;
            this.date = new Date();
            this.customStatusCode = customStatusCode;
            this.httpStatus = httpStatus;
            this.details = details;
        }
    }
}
