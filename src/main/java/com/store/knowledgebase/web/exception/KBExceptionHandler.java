package com.store.knowledgebase.web.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
@ControllerAdvice
public class KBExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(KBException.class)
    public ResponseEntity<Object> handleCustomException(KBException ex, WebRequest request) {

        return new ResponseEntity<>(ex.getExceptionResponse(request.getDescription(false)),
                ex.getKBExceptionEnum().getHttpStatusCode());
    }
}
