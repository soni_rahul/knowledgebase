package com.store.knowledgebase.web.exception;

import org.springframework.http.HttpStatus;

public enum KBExceptionEnum {

    BOOK_NOT_FOUND(10000, HttpStatus.NOT_FOUND, "book not found");

    private final int kbStatusCode;
    private final HttpStatus httpStatusCode;
    private final String reasonPhrase;

    KBExceptionEnum(int kbStatusCode, HttpStatus httpStatusCode, String reasonPhrase) {
        this.kbStatusCode = kbStatusCode;
        this.httpStatusCode = httpStatusCode;
        this.reasonPhrase = reasonPhrase;
    }

    public int getKbStatusCode() {
        return kbStatusCode;
    }

    public HttpStatus getHttpStatusCode() {
        return httpStatusCode;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }
}
