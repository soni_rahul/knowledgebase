package com.store.knowledgebase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class KnowledgebaseApplication {

    private static final Logger log = LoggerFactory.getLogger(KnowledgebaseApplication.class);

    private final Environment env;

    public KnowledgebaseApplication(Environment env) {
        this.env = env;
    }

    public static void main(String[] args) {

        ApplicationContext context = SpringApplication.run(KnowledgebaseApplication.class, args);

        Environment env = context.getEnvironment();

        log.info("\n----------------------------------------------------------\n\t" +
                        "Application '{}' is running! Access URLs:\n\t" +
                        "Local: \t\thttp://localhost:{}{}\n\t" +
                        "Profile(s): \t{}" +
                        "\n----------------------------------------------------------",
                env.getProperty("spring.application.name"),
                env.getProperty("server.port"),
                env.getProperty("server.servlet.context-path"),
                env.getActiveProfiles());
    }

}
