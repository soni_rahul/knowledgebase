package com.store.knowledgebase.service;

import com.store.knowledgebase.domain.Author;

import java.util.List;

public interface AuthorService {

    Author save(Author Author);

    Author findById(Long id);

    List<Author> findAll();

    void deleteById(Long id);
}
