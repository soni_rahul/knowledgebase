package com.store.knowledgebase.service.impl;

import com.store.knowledgebase.domain.Author;
import com.store.knowledgebase.repository.AuthorRepository;
import com.store.knowledgebase.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService {

    private AuthorRepository authorRepository;

    public AuthorServiceImpl(AuthorRepository repository) {
        this.authorRepository = repository;
    }

    public Author save(Author Author) {
        return authorRepository.save(Author);
    }

    public Author findById(Long id) {
        return authorRepository.findById(id).orElse(null);
    }

    public List<Author> findAll() {
        Iterable<Author> AuthorIterator = authorRepository.findAll();


        List<Author> Authors = new ArrayList<>();
        for (Author itr : AuthorIterator) {
            Authors.add(itr);
        }
        return Authors;
    }

    public void deleteById(Long id) {
        authorRepository.deleteById(id);
    }
}
