package com.store.knowledgebase.service.impl;

import com.store.knowledgebase.domain.Book;
import com.store.knowledgebase.repository.BookRepository;
import com.store.knowledgebase.service.BookService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book save(Book book) {
        return bookRepository.save(book);
    }

    public Optional<Book> findById(Long id) {
        return bookRepository.findById(id);
    }

    public List<Book> findAll() {
        Iterable<Book> bookIterator = bookRepository.findAll();

        List<Book> books = new ArrayList<>();
        for (Book itr : bookIterator) {
            books.add(itr);
        }
        return books;
    }

    public void deleteById(Long id) {
        bookRepository.deleteById(id);
    }
}
